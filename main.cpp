/**
 * Testcase for different mixture of strided and non-strided memory accesses
 */

#include <iostream>
#include <omp.h>

#define SEG 64
#define STRIDE 4096
#define ALIGNMENT 4096
#define N 8192
#define UNTIL 30

int main(int argc, char **argv) {
  int nthreads = omp_get_num_procs();
  int st = 0, ed = nthreads;
  if (argc > 1) {
    st = std::stoi(argv[1]);
    ed = std::stoi(argv[2]);
  }
  double time[nthreads];
  double speed[nthreads];
  long size[nthreads];
  omp_set_dynamic(0);
  std::cout << "Total number of threads " << nthreads << std::endl;
  char *outptr[nthreads];
  char *inptr[nthreads];
  for (int i = 0; i < nthreads; ++i) {
    outptr[i] = (char *) aligned_alloc(ALIGNMENT, N * STRIDE);
    inptr[i] = (char *) aligned_alloc(ALIGNMENT, N * STRIDE);
  }
  for (int stride_thread = st; stride_thread <= ed; ++stride_thread) {
#pragma omp parallel
    {
      int threadIdx = omp_get_thread_num();
      char * in = inptr[threadIdx];
      char * out = outptr[threadIdx];
#pragma omp barrier
      double st = omp_get_wtime();
      double ed;
      long cnt = 0;
      if (threadIdx < stride_thread) {
        do {
          long opos = 0, ipos = 0;
          for (long ipos_st = 0; ipos_st < STRIDE; ipos_st += SEG) {
            ipos = ipos_st;
            for (long j = 0; j < N; ++j) {
#pragma omp simd aligned(in, out: ALIGNMENT)
              for (long i = 0; i < SEG; ++i)
                out[opos + i] = in[ipos + i];
              opos += SEG;
              ipos += STRIDE;
            }
          }
          ed = omp_get_wtime();
          ++cnt;
        } while (ed < st + UNTIL);
        time[threadIdx] = ed - st;
        size[threadIdx] = cnt * N * STRIDE * 2;
        speed[threadIdx] = size[threadIdx] / time[threadIdx] * 1e-9;
      } else {
        do {
#pragma omp simd aligned(in, out: ALIGNMENT)
          for (long i = 0; i < N * STRIDE; ++i)
            out[i] = in[i];
          ed = omp_get_wtime();
          ++cnt;
        } while (ed < st + UNTIL);
        time[threadIdx] = ed - st;
        size[threadIdx] = cnt * N * STRIDE * 2;
        speed[threadIdx] = size[threadIdx] / time[threadIdx] * 1e-9;
      }
    }
    double tot_speed = 0;
    for (int i = 0; i < nthreads; ++i)
      tot_speed += speed[i];
    std::cout << "Number of threads striding " << stride_thread << "\t" << tot_speed << "GB/s" << std::endl;
    for (int i = 0; i < nthreads; ++i)
      std::cout << "thread " << time[i] << "s\t" << size[i] * 1.0e-9 << "GB\t" << speed[i] << "GB/s" << std::endl;
  }
  return 0;
}
